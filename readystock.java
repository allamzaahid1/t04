class readystock extends kue{
    //untuk mengatasi abstract dari class induk (kue), membuat variabel jumlah dan berat berupa double
    private double Jumlah;
    private double Berat;

    //menggunakan konstruktor untuk mengisi nama, harga, dan jumlah
    public readystock(String name, double price, int jumlah) {
        //menggunakan superclass untuk mengisi nama dan harga yang merupakan variabel dari class induk (kue)
        super(name, price);
        this.Jumlah = jumlah;
    }
    
    //untuk mendapatkan jumlah dari kue, menggunakan nama method "Jumlah" untuk menyesuaikan abstract pada class induk (kue)
    @Override
    public double Jumlah(){
        return this.Jumlah;
    }

    //hanya untuk mengatasi abstract dari class induk (kue) harus ada method "Berat", tetapi tidak digunakan
    @Override
    public double Berat(){
        return Berat;
    }

    //untuk menghitung harga, menggunakan nama method "hitungHarga" untuk menyesuaikan abstract pada class induk (kue)
    @Override
    public double hitungHarga(){
        return getPrice() * Jumlah() * 2;
    }

}