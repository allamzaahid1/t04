public class pesanan extends kue{
    //untuk mengatasi abstract dari class induk (kue), membuat variabel jumlah dan berat berupa double
    private double Berat;
    private double Jumlah;

    //menggunakan konstruktor untuk mengisi nama, harga, dan berat
    public pesanan(String name, double price, double berat) {
        //menggunakan superclass untuk mengisi nama dan harga yang merupakan variabel dari class induk (kue)
        super(name, price);
        this.Berat = berat;
    }

    //untuk mendapatkan berat dari kue, menggunakan nama method "Berat" untuk menyesuaikan abstract pada class induk (kue)
    @Override
    public double Berat(){
        return this.Berat;
    }

    //hanya untuk mengatasi abstract dari class induk (kue) harus ada method "Jumlah", tetapi tidak digunakan
    @Override
    public double Jumlah(){
        return Jumlah;
    }

    //untuk menghitung harga, menggunakan nama method "hitungHarga" untuk menyesuaikan abstract pada class induk (kue)
    @Override
    public double hitungHarga() {
        return getPrice() * Berat();
    }
 
}